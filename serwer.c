/******************************************************************************
 * Remote SCTP bash executor                                                  *
 ******************************************************************************/

/**
 * @file serwer.c
 * @author Mateusz Kozyra
 * @date Feb 2022
 * @brief Server code
 * @ver 0.1
 */

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/sctp.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <netdb.h>
#include <unistd.h>
#include <errno.h>

// params
#define PORT 2016
#define MAX_INPUT_SIZE  512
#define MAX_OUTPUT_SIZE 1024

int main(){

printf("\
 _               _                                    \n\
| |__   __ _ ___| |__    ___  ___ _ ____   _____ _ __ \n\
| '_ \\ / _` / __| '_ \\  / __|/ _ \\ '__\\ \\ / / _ \\ '__|\n\
| |_) | (_| \\__ \\ | | | \\__ \\  __/ |   \\ V /  __/ |   \n\
|_.__/ \\__,_|___/_| |_| |___/\\___|_|    \\_/ \\___|_|   \n\
\n");         

/////////////// INIT - SCTP 
  struct sockaddr_in server, client;
  int sock;

  // socket
  if((sock = socket(AF_INET, SOCK_STREAM, IPPROTO_SCTP)) == -1){
    printf("Error create socket!\n");
    return -1;
  }else printf("Socket created\n\r");

  // settings
  server.sin_family = AF_INET;
  server.sin_addr.s_addr = htonl(INADDR_ANY); // local ip
  server.sin_port = htons(PORT); // port

  // bind
  if(bind(sock, (struct sockaddr*)&server, sizeof(server)) == -1){
    printf("Error bind!\n");
    return -1;
  }

  if(listen(sock,5)==-1){
    printf("Error listen!\n");
    return -1;
  }

/////////////// INIT - SCTP

  int newsock;
  char cmd[MAX_INPUT_SIZE];
  int clnlen = sizeof(client);
  int flags;

/////////////// LOOP
  while(1){

    // waiting for cients
    if((newsock = accept(sock, (struct sockaddr*)&client, &clnlen)) == -1){
      printf("Error accept!\n");
      return -1;
    } else printf("New client!\n");

    struct sctp_sndrcvinfo sndrcvinfo;

    memset(cmd, 0, MAX_INPUT_SIZE);
    if(sctp_recvmsg(newsock, (void*)cmd, (size_t)MAX_INPUT_SIZE, (struct sockaddr*)NULL, 0, &sndrcvinfo, &flags) == -1){ // recv
      printf("Error sctp_recvmsg!\n");
      return -1;
    } else printf("New cmd: %s\n\r", cmd);

    // pipe to communicate processes
    int filedes[2];
    if (pipe(filedes) == -1) {
      perror("Pipe Failed");
      exit(1);
    }

    // fork - child process
    pid_t pid = fork();
    if (pid == -1) {
      perror("fork Failed");
      exit(1);
    } 
    
    // child
    else if (pid == 0) {
        printf("Child process\n\r");
        char *args[3];
        char * pch;

        // prepare data
        pch = strtok (cmd," ");
        args[0] = pch;
        pch = strtok (NULL," ");
        args[1] = pch;
        pch = strtok (NULL," ");
        args[2] = pch;
        printf("args: %s %s %s\n\r", args[0], args[1], args[2]);

        // standard output => to pipe
        while ((dup2(filedes[1], STDOUT_FILENO) == -1) && (errno == EINTR)) {}
        // close pipe
        close(filedes[1]);
        close(filedes[0]);

        // execute
        if(execvp(cmd, args) == -1){
          printf("CMD ERROR\n");
        }; 
        exit(0);
    } 

    // main process
    else{
        // close for writing
        close(filedes[1]);
        // wait for child
        wait(NULL);
        // get data
        char buffer[4096];
        memset(buffer, 0, 4096);
        ssize_t count=0;
        count = read(filedes[0], buffer, sizeof(buffer));
        if (count == -1) {
            if (errno == EINTR) continue;
            else {
                perror("pipe read Failed");
                exit(1);
            }

        } else if (count == 0) {
            break;

        } else {
            // send data
            if(sctp_sendmsg(newsock, (void*)buffer, 4096, NULL, 0, 0, 0, 1, 0, 0) == -1){ 
                printf("Error sctp_sendmsg! %s\n", strerror(errno));
                return -1;
            }
        }
        // close for read
        close(filedes[0]);
        close(newsock); // close connection
    }
  }

  close(sock); // close socket
  return 0;
}
/////////////// LOOP
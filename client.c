/******************************************************************************
 * Remote SCTP bash executor                                                  *
 ******************************************************************************/

/**
 * @file client.c
 * @author Mateusz Kozyra
 * @date Feb 2022
 * @brief Client
 * @ver 0.1
 */


#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/sctp.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <netdb.h>
#include <unistd.h>
#include <errno.h>
#include <stdio.h>

// server params
#define PORT        2016
#define IP_ADDRESS "127.0.0.1"

int main(){

  // server struct
  struct sockaddr_in server;
  server.sin_addr.s_addr = inet_addr(IP_ADDRESS);
  server.sin_family = AF_INET;
  server.sin_port = htons(PORT);

  // socket
  int sock;
  if((sock = socket(AF_INET, SOCK_STREAM, IPPROTO_SCTP)) == -1){
    printf("Error create!\n");
    return -1;
  }

  // connect to server
  if(connect(sock, (struct sockaddr*)&server, sizeof(server)) == -1){
    printf("Error connect!\n");
  }

  char cmd[512];
  printf(">> ");
  scanf("%[^\n]s",cmd);// user data

  // send cmd
  sctp_sendmsg(sock, (void*)cmd, (size_t)strlen(cmd), NULL, 0, 0, 0, 1, 0, 0);

  struct sctp_sndrcvinfo sndrcvinfo;
  int i, flags;
  char buff[512];
  for(i = 0; i < 1; i++){ // recv
    bzero((void *)&buff, sizeof(buff));
    sctp_recvmsg(sock, (void*)buff, sizeof(buff), (struct sockaddr*)NULL, 0, &sndrcvinfo, &flags);
    printf("Server: %s\n",buff);
  }
  
  // close socket
  close(sock);
  return 0;
}